﻿using NUnit.Framework;

namespace Lekmer.Framework.Setting.UnitTest.Setting
{
	[TestFixture]
	public class SettingManyGroupsTest
	{
		[Test]
		public void GetAllGroups_Execute_NoGroupsReturned()
		{
			var setting = new SettingManyGroups();

			var allGroups = setting.GetAllGroups();

			Assert.IsNotNull(allGroups, "Groups Collection");
			Assert.AreEqual(9, allGroups.Count, "Groups Collection Count");
		}
	}
}
