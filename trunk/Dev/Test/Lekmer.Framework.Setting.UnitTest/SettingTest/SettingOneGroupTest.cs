﻿using NUnit.Framework;

namespace Lekmer.Framework.Setting.UnitTest.Setting
{
	[TestFixture]
	public class SettingOneGroupTest
	{
		[Test]
		public void GetAllGroups_Execute_NoGroupsReturned()
		{
			var setting = new SettingOneGroup();

			var allGroups = setting.GetAllGroups();

			Assert.IsNotNull(allGroups, "Groups Collection");
			Assert.AreEqual(1, allGroups.Count, "Groups Collection Count");
		}
	}
}
