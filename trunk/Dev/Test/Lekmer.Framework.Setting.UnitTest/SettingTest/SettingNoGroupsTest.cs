﻿using NUnit.Framework;

namespace Lekmer.Framework.Setting.UnitTest.Setting
{
	[TestFixture]
	public class SettingNoGroupsTest
	{
		[Test]
		public void GetAllGroups_Execute_NoGroupsReturned()
		{
			var setting = new SettingNoGroups();

			var allGroups = setting.GetAllGroups();

			Assert.IsNotNull(allGroups, "Groups Collection");
			Assert.AreEqual(0, allGroups.Count, "Groups Collection Count");
		}
	}
}
