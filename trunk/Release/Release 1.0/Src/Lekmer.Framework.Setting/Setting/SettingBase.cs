using System.Collections.ObjectModel;

namespace Lekmer.Framework.Setting
{
	/// <summary>
	/// Base class for settings.
	/// </summary>
	public abstract class SettingBase : Litium.Framework.Setting.SettingBase
	{
		private ISettingRepository _repository;

		protected ISettingRepository Repository
		{
			get { return _repository ?? (_repository = CreateRepositoryInstance()); }
		}


		/// <summary>
		/// Gets list of group names from the settings.
		/// </summary>
		/// <returns>List of group names.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<string> GetAllGroups()
		{
			Collection<string> groups;
			if (!GroupsCache.GetGroups(StorageName, out groups))
			{
				groups = Repository.GetAllGroups(StorageName);

				GroupsCache.AddGroups(StorageName, groups);
			}

			return groups;
		}


		/// <summary>
		/// Creates a repository instance.
		/// </summary>
		/// <returns>Instance of <see cref="ISettingRepository"/>.</returns>
		protected virtual new ISettingRepository CreateRepositoryInstance()
		{
			return new XmlFileSettingRepository();
		}
	}
}