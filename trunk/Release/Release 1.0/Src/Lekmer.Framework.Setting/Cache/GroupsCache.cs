using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Lekmer.Framework.Setting
{
	/// <summary>
	/// Handles caching of setting groups locally in memory.
	/// </summary>
	public static class GroupsCache
	{
		private const int _cacheExpirationTime = 10;
		private static readonly Dictionary<string, Collection<string>> _cache = new Dictionary<string, Collection<string>>();
		private static readonly object _lockObject = new object();
		private static DateTime _cacheLastUpdate = DateTime.UtcNow;

		private static bool CacheIsOld
		{
			get { return DateTime.UtcNow > _cacheLastUpdate.AddMinutes(_cacheExpirationTime); }
		}

		private static void ClearCacheWhenExpired()
		{
			if (CacheIsOld)
			{
				lock (_lockObject)
				{
					if (CacheIsOld)
					{
						_cache.Clear();
						_cacheLastUpdate = DateTime.UtcNow;
					}
				}
			}
		}

		internal static bool GetGroups(string storageName, out Collection<string> groups)
		{
			ClearCacheWhenExpired();

			return _cache.TryGetValue(storageName, out groups);
		}

		internal static void AddGroups(string storageName, Collection<string> groups)
		{
			lock (_lockObject)
			{
				_cache[storageName] = groups;
			}
		}

		/// <summary>
		/// Clears the setting cache.
		/// </summary>
		public static void Clear()
		{
			lock (_lockObject)
			{
				_cache.Clear();
			}
		}
	}
}