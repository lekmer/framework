using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Xml.XPath;

namespace Lekmer.Framework.Setting
{
	/// <summary>
	/// Repository that reads settings from xml-files.
	/// </summary>
	public class XmlFileSettingRepository : Litium.Framework.Setting.XmlFileSettingRepository, ISettingRepository
	{
		private const string _configurationErrorMessage = @"Error occured when trying to read SettingXmlFilePathTemplate from App.config. Please make sure the AppSetting key SettingXmlFilePathTemplate is set. (Example: <appSettings><add key=""SettingXmlFilePathTemplate"" value=""C:\MyProject\Setting\{0}.xml""></appSettings>)";
		private static string _settingFilePathTemplate;


		/// <summary>
		/// Gets list of group names from the setting repository.
		/// </summary>
		/// <returns>List of group names.</returns>
		public virtual Collection<string> GetAllGroups(string storageName)
		{
			var groupNames = new Collection<string>();

			using (StreamReader reader = GetSettingDocument(storageName))
			{
				IXPathNavigable settingDocument = new XPathDocument(reader);
				XPathNavigator settingNavigator = settingDocument.CreateNavigator();

				string xpathQuery = string.Format(CultureInfo.InvariantCulture, "Root/Group");

				XPathNodeIterator groups = settingNavigator.Select(xpathQuery);
				while (groups.MoveNext())
				{
					var group = groups.Current;
					if (group != null)
					{
						string groupName = @group.GetAttribute("Name", String.Empty);
						groupNames.Add(groupName);
					}
				}
			}

			return groupNames;
		}


		protected virtual string RetrieveSettingFilePathTemplate()
		{
			if (_settingFilePathTemplate != null)
			{
				return _settingFilePathTemplate;
			}

			try
			{
				_settingFilePathTemplate = ConfigurationManager.AppSettings["SettingXmlFilePathTemplate"];
			}
			catch (Exception ex)
			{
				throw new ConfigurationErrorsException(_configurationErrorMessage, ex);
			}

			if (_settingFilePathTemplate == null)
			{
				throw new ConfigurationErrorsException(_configurationErrorMessage);
			}

			return _settingFilePathTemplate;
		}

		protected virtual string GetSettingFilePath(string storageName)
		{
			return string.Format(CultureInfo.InvariantCulture, RetrieveSettingFilePathTemplate(), storageName);
		}

		protected virtual StreamReader GetSettingDocument(string storageName)
		{
			string settingFilePath = GetSettingFilePath(storageName);
			string fullPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, settingFilePath));

			if (!File.Exists(fullPath))
			{
				throw new ConfigurationErrorsException(
					string.Format(CultureInfo.InvariantCulture, "The setting file '{0}' does not exists.", fullPath));
			}

			return File.OpenText(fullPath);
		}
	}
}