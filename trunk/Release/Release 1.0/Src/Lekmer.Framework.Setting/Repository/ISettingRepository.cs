using System.Collections.ObjectModel;

namespace Lekmer.Framework.Setting
{
	/// <summary>
	/// Setting repository interface to be implemented to get a custom repository.
	/// </summary>
	public interface ISettingRepository : Litium.Framework.Setting.ISettingRepository
	{
		/// <summary>
		/// Gets list of group names from the setting repository.
		/// </summary>
		/// <returns>List of group names.</returns>
		Collection<string> GetAllGroups(string storageName);
	}
}