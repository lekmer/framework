﻿namespace Lekmer.Framework.Setting.UnitTest.Setting
{
	public class SettingNoGroups : SettingBase
	{
		protected override string StorageName
		{
			get { return "NoGroups"; }
		}
	}
}
