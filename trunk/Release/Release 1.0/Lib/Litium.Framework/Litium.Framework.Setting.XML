<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Litium.Framework.Setting</name>
    </assembly>
    <members>
        <member name="T:Litium.Framework.Setting.SettingCache">
            <summary>
            Handles caching of settings locally in memory.
            </summary>
        </member>
        <member name="M:Litium.Framework.Setting.SettingCache.Clear">
            <summary>
            Clears the setting cache.
            </summary>
        </member>
        <member name="T:Litium.Framework.Setting.XmlFileSettingRepository">
            <summary>
            Repository that reads settings from xml-files.
            </summary>
        </member>
        <member name="T:Litium.Framework.Setting.ISettingRepository">
            <summary>
            Setting repository interface to be implemented to get a custom repository.
            </summary>
        </member>
        <member name="M:Litium.Framework.Setting.ISettingRepository.GetValue(System.String,System.String,System.String,System.Boolean)">
            <summary>
            Gets a value from the setting repository.
            </summary>
            <author>Erik Juhlin</author>
            <date>2008-04-15</date>
            <param name="storageName">Where the settings are stored.</param>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="throwExceptionWhenNotFound">
            If set to true an exception will be thrown if the setting can't be found.
            If set to false, null will be returned.
            </param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found and throwExceptionWhenNotFound set to true.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.XmlFileSettingRepository.GetValue(System.String,System.String,System.String,System.Boolean)">
            <summary>
            Gets a value from the setting repository.
            </summary>
            <author>Erik Juhlin</author>
            <date>2008-04-15</date>
            <param name="storageName">Where the settings are stored.</param>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="throwExceptionWhenNotFound">
            If set to true an exception will be thrown if the setting can't be found.
            If set to false, null will be returned.
            </param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found and throwExceptionWhenNotFound set to true.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="T:Litium.Framework.Setting.SettingBase">
            <summary>
            Base class for settings.
            </summary>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.CreateRepositoryInstance">
            <summary>
            Creates a repository instance.
            </summary>
            <returns>Instance of <see cref="T:Litium.Framework.Setting.ISettingRepository"/>.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetValue(System.String,System.String)">
            <summary>
            Gets a setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetValue(System.String,System.String,System.Boolean)">
            <summary>
            Gets a setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="throwExceptionWhenNotFound">
            If set to true an exception will be thrown if the setting can't be found.
            If set to false, null will be returned.
            </param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found and throwExceptionWhenNotFound set to true.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetInt32(System.String,System.String)">
            <summary>
            Gets an Int32 setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not an Int32.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetInt32(System.String,System.String,System.Int32)">
            <summary>
            Gets an Int32 setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not an Int32.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetInt64(System.String,System.String)">
            <summary>
            Gets an Int64 setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not an Int64.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetInt64(System.String,System.String,System.Int64)">
            <summary>
            Gets an Int64 setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not an Int64.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetSingle(System.String,System.String)">
            <summary>
            Gets a single setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not a single.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetSingle(System.String,System.String,System.Single)">
            <summary>
            Gets a double setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not a single.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetDouble(System.String,System.String)">
            <summary>
            Gets a double setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not a double.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetDouble(System.String,System.String,System.Double)">
            <summary>
            Gets a double setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not a double.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetDecimal(System.String,System.String)">
            <summary>
            Gets a decimal setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not a decimal.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetDecimal(System.String,System.String,System.Decimal)">
            <summary>
            Gets a decimal setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not a decimal.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetBoolean(System.String,System.String)">
            <summary>
            Gets a boolean setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found or setting is not a boolean.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetBoolean(System.String,System.String,System.Boolean)">
            <summary>
            Gets a boolean setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found or not a boolean.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetString(System.String,System.String,System.String)">
            <summary>
            Gets a string setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetString(System.String,System.String)">
            <summary>
            Gets a string setting value.
            </summary>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <exception cref="T:System.Configuration.ConfigurationErrorsException">Thrown when group or setting not found.</exception>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetTypedValue``1(System.String,System.String,Litium.Framework.Setting.SettingBase.TryParseCallback{``0})">
            <summary>
            Get a setting casted to a certain type.
            </summary>
            <typeparam name="T">Type of the setting.</typeparam>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="tryParseCallback">Tryparse callback method.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetTypedNumberValue``1(System.String,System.String,Litium.Framework.Setting.SettingBase.TryParseNumberCallback{``0})">
            <summary>
            Get a setting casted to a certain type.
            </summary>
            <typeparam name="T">Type of the setting.</typeparam>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="tryParseCallback">Tryparse callback method.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetTypedValue``1(System.String,System.String,``0,Litium.Framework.Setting.SettingBase.TryParseCallback{``0})">
            <summary>
            Get a setting casted to a certain type.
            </summary>
            <typeparam name="T">Type of the setting.</typeparam>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found.</param>
            <param name="tryParseCallback">Tryparse callback method.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="M:Litium.Framework.Setting.SettingBase.GetTypedNumberValue``1(System.String,System.String,``0,Litium.Framework.Setting.SettingBase.TryParseNumberCallback{``0})">
            <summary>
            Get a setting casted to a certain type.
            </summary>
            <typeparam name="T">Type of the setting.</typeparam>
            <param name="group">Group where the setting is.</param>
            <param name="name">Name of the setting.</param>
            <param name="defaultValue">Returned when setting not found.</param>
            <param name="tryParseCallback">Tryparse callback method.</param>
            <returns>Value of the setting.</returns>
        </member>
        <member name="P:Litium.Framework.Setting.SettingBase.StorageName">
            <summary>
            Name of setting storage.
            </summary>
        </member>
        <member name="T:Litium.Framework.Setting.SettingBase.TryParseCallback`1">
            <summary>
            Callback method called when wanting to do a TryParse for a certain type.
            </summary>
            <typeparam name="T">The type to do the TryParse for.</typeparam>
            <param name="value">The value to parse.</param>
            <param name="result">Should return the parsed value. default value for the T type if not parseable.</param>
            <returns>True if the value is parsable. False if not.</returns>
        </member>
        <member name="T:Litium.Framework.Setting.SettingBase.TryParseNumberCallback`1">
            <summary>
            Callback method called when wanting to do a TryParse for a certain type.
            </summary>
            <typeparam name="T">The type to do the TryParse for.</typeparam>
            <param name="value">The value to parse.</param>
            <param name="style">A bitwise combination of enumeration values that indicates the style elements that can be present in <paramref name="value"/>. A typical value to specify is <see cref="F:System.Globalization.NumberStyles.Integer"/>.</param>
            <param name="provider">An object that supplies culture-specific formatting information about <paramref name="value"/>.</param>
            <param name="result">Should return the parsed value. default value for the T type if not parseable.</param>
            <returns>True if the value is parsable. False if not.</returns>
        </member>
    </members>
</doc>
